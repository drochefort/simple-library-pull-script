var pack = require('./package');

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks 

  grunt.initConfig({
    versions: pack.hmhDependencies,
    fetchlib: {
      options: {
        baseUrl: 'https://bitbucket.org/tribalnova/hmh-hab-mtl-widgets-common/downloads/',
        basePath: 'assets/widgets/shared/js/',
        auth: require('./.auth')
      },
      widgetsCommon: {
        files: [{
          dest: 'hab-mtl-widgets-common.js',
          src: 'hab-mtl-widgets-common.<%=versions.widgetsCommon%>.min.js'
        }]
      }
    }
  })

  // Update the common library
  grunt.registerTask('default', ['fetchlib']);
}